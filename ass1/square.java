import java.util.Scanner;

public class square {
    private static Scanner sc;
    public static void main(String[] args)
    {
        int a;
        sc = new Scanner(System.in);
        System.out.print("Please Entert Number Between 1 and 20: ");
        a = sc.nextInt();
        while(a<1 && a>20){
            System.out.print("Please Entert Number Between 1 and 20: ");
            a = sc.nextInt();
        }
        for(int i=0;i<a;i++){
            System.out.print("*");
        }
        System.out.print("\n");
        for(int i=0;i<a-2;i++){
            System.out.print("*");
            for(int j=0;j<a-2;j++){
                System.out.print(" ");
            }
            System.out.print("*");
            System.out.print("\n");
        }
        for(int i=0;i<a;i++){
            System.out.print("*");
        }
    }
}
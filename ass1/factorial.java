import java.util.Scanner;

public class factorial {
    private static Scanner sc;
    public static void main(String[] args)
    {
        long a=0;
        for(int i=1;i<=20;i++){
            a=1;
            for(int j=1;j<=i;j++){
                a*=j;
            }
            System.out.print("Factorial of " +i + " is : " +a);
            System.out.print("\n");
        }
        System.out.print("factorial(100) has more than 32 binary zeros on the end, so converting it to an integer produces zero.");

    }
}
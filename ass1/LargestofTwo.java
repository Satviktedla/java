import java.util.Scanner;

public class LargestofTwo {
    private static Scanner sc;
    public static void main(String[] args)
    {
        int a,b;
        sc = new Scanner(System.in);

        System.out.print(" Please Enter the First Number : ");
        a = sc.nextInt();

        System.out.print(" Please Enter the Second Number : ");
        b = sc.nextInt();

        if(a > b)
        {
            System.out.println(a + " is Larger ");
        }
        else if (a < b)
        {
            System.out.println(b + " is Larger ");
        }
        else
        {
            System.out.println(" These numbers are equal");
        }
    }
}
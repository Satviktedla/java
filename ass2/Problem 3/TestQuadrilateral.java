public class TestQuadrilateral{
    public static void main(String[] args){
        Square sq=new Square(0,0,10,0,10,10,0,10);
        System.out.println("Area of the Square is " + sq.area());
        Rectangle rec=new Rectangle(0,0,20,0,20,10,0,10);
        System.out.println("Area of the Rectangle is " + rec.area());
        Parallelogram pa=new Parallelogram(10,10,30,10,20,20,0,20,8);
        System.out.println("Area of the Parallelogram is " + pa.area());
        Trapezoid t=new Trapezoid(10,10,30,10,40,20,0,20,8);
        System.out.println("Area of the Trapezoid is " + t.area());
    }

}
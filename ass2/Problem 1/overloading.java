
class overloading{
    public void run()
    {
        System.out.println("with out parameter");
    }
    public void run(int i)
    {
        System.out.println("With parameter");
    }
    public static void main(String[] args)
    {
        overloading t = new overloading();
        t.run(1);
    }
}

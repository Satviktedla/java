class Dog{
    public void bark(){
        System.out.println("woof ");
    }
}
class Animal extends Dog{
    public void sniff(){
        System.out.println("sniff ");
    }

    public void bark(){
        System.out.println("bowl");
    }
}

public class overriding {
    public static void main(String [] args){
        Dog dog = new Animal();
        dog.bark();
    }
}